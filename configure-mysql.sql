# connect to mysal and run as root user
# Create Databases
CREATE DATABASE news_site;

# Create user
CREATE USER 'news_user'@'localhost' IDENTIFIED BY 'P@$$w0rd';

# Database Grants
GRANT SELECT,INSERT,UPDATE,DELETE ON news_site.* to 'news_user'@'localhost';

