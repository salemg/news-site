package com.project.newssite;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
@Entity
@Table(name="news")
public class News {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@NotBlank
	private String title;
	
	@NotBlank
	private String description;
	
	private String picture;
	
	@Enumerated(EnumType.STRING)
	Category category;
	
	public enum Category{ SPORT, TECHNOLOGY, ENTERTAINMENT, BREAKINGNEWS ,BUSINESS }
}
