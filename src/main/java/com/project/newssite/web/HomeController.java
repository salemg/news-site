package com.project.newssite.web;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.project.newssite.News;
import com.project.newssite.News.Category;
import com.project.newssite.repositories.NewsRepository;

@Controller
public class HomeController {
	private NewsRepository newsRepository;
	
	@Autowired
	public HomeController(NewsRepository newsRepository){
		this.newsRepository = newsRepository;
	}

	@ModelAttribute
	public void addNewsToModel(Model model) {
		
		Category[] categories = News.Category.values();
		for(Category category : categories) {
			List<News> newsList = new ArrayList<>();
			
			newsRepository.findNewsByCategory(category).forEach(i->newsList.add(i));
			
			model.addAttribute(category.toString(), newsList);
			
		}
	}
	
	
	@GetMapping("/")
	public String home() {
		
		return "index";
		
	}
	
}
