package com.project.newssite.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.project.newssite.News;
import com.project.newssite.News.Category;

public interface NewsRepository extends CrudRepository<News,Long>{
	
//	@Query("select n from News n where n.category = ?")
	public List<News> findNewsByCategory(Category category);
}
